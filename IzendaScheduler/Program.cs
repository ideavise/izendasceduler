﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace IzendaScheduler
{
    class Program
    {
        static void Main(string[] args)
        {
            args = new string[] { "http://reports.crushwave.com/rs.aspx" };
            if (args.Length < 1)
            {
                Console.WriteLine("Usage: IzendaScheduler ResponseServerPath [Minutes] [u:UserName] [p:Password]\nSample: IzendaScheduler http://domain.com/reports/rs.aspx 1 u:Admin p:123\n");
            }
            else
            {
                string str1 = args[0];
                string str2 = "1";
                string userName = "";
                string password = "";
                for (int index = 1; index < args.Length; ++index)
                {
                    if (args[index].StartsWith("u:"))
                        userName = args[index].Substring(2);
                    else if (args[index].StartsWith("p:"))
                    {
                        password = args[index].Substring(2);
                    }
                    else
                    {
                        int result;
                        if (int.TryParse(args[index], out result))
                            str2 = args[index];
                    }
                }
                string str3;
                try
                {
                    using (CustomWebClient customWebClient = new CustomWebClient())
                    {
                        string address = string.Format("{0}?run_scheduled_reports={1}&username=", (object)str1, (object)str2, userName);

                        Stream stream = customWebClient.OpenRead(address);
                        using (StreamReader streamReader = new StreamReader(stream))
                            str3 = streamReader.ReadToEnd().Replace("<br>", Environment.NewLine).Replace("<br/>", Environment.NewLine);
                        stream.Close();

                        stream = customWebClient.OpenRead(address);
                        using (StreamReader streamReader = new StreamReader(stream))
                            str3 = streamReader.ReadToEnd().Replace("<br>", Environment.NewLine).Replace("<br/>", Environment.NewLine);
                        stream.Close();
                    }
                }
                catch (Exception ex)
                {
                    str3 = "Can not schedule reports: " + ex.Message;
                }
                Console.Write(str3);
            }
        }
    }

    public class CustomWebClient : WebClient
    {
        private readonly CookieContainer m_container = new CookieContainer();

        public CustomWebClient()
        {
            try
            {
                NetworkCredential cred = CredentialCache.DefaultNetworkCredentials;
                if (cred != null)
                {
                    this.UseDefaultCredentials = false;
                    this.Credentials = cred;
                }
            }
            catch { }
        }

        protected override WebRequest GetWebRequest(Uri address)
        {
            WebRequest request = base.GetWebRequest(address);
            HttpWebRequest webRequest = request as HttpWebRequest;
            if (webRequest != null)
                webRequest.CookieContainer = m_container;
            request.PreAuthenticate = true;
            return request;
        }
    }
}